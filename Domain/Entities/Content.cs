﻿using System;
using System.Collections.Generic;

namespace Domain.Entities;

public partial class Content
{
    public Guid ContentId { get; set; }

    public string? Titulo { get; set; }

    public string? Area { get; set; }

    public string? Autor { get; set; }

    public string? Descricao { get; set; }

    public DateTime? DataPublicacao { get; set; }
}
