# AEC

Desafio AeC Automação

- Linguagem: .NET CORE 7
- Banco SQL Server


## Requisitos

- Ambiente .Net CORE 7
- Ambiente SQL Server
- Driver do Chrome driver presente na pasta c:/ChromeDriver
- Chrome v111
- Rodar Script da Estrutura da tabela no banco SQL Server


## Bibliotecas e ferrramentas


| Ferramentas | Motivo |
| ------ | ------ |
| .Net Core API | Tipo de projeto que mais tenho trabalhado ultimamente. |
| Swagger | Facilidade para a realização de testes e boa documentação. |
| Sql Server | Banco de dados que eu tenho mais vivência. |
| Entity Framework | Facilidade e praticidade para mapear e realizar as operações basicas de Banco de Dados |


## Notas

A arquitetura do Sistema escolhida foi a Clean Architecture. É uma arquitetura que eu gosto e tenho utilizado nos meus projetos e que garante os requisitos d DDD e Injeção de Dependência.





