﻿using Domain.Entities;
using Domain.Interfaces;
using Infrastructure.Context;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class ContentRepository : IContentRepository
    {
        private string _connection;
        AECContext _context;
        public ContentRepository(IConfiguration configuration) {
            _connection = configuration.GetConnectionString("LocalString");
            _context = new AECContext(_connection);
        }

        public void SaveContent(Content content)
        {
            
                _context.Contents.Add(content); 
                _context.SaveChanges();
           
        }
    }
}
