﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModel
{
    public class ContentModel
    {
        public Guid ContentId { get; set; }

        public string? Titulo { get; set; }

        public string? Area { get; set; }

        public string? Autor { get; set; }

        public string? Descricao { get; set; }

        public DateTime? DataPublicacao { get; set; }

    }
}
