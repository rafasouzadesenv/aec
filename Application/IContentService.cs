﻿using Application.Helper;
using Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public interface IContentService
    {

        ResponseMessage? SearchContent(string term);
    }
}
