﻿using Application.Helper;
using Application.ViewModel;
using AutoMapper;
using Domain.Entities;
using Domain.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.DevTools.V109.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Application.Content
{
    public class ContentService : IContentService
    {
        private ChromeDriver _driver;
        private IContentRepository _contentRepository;
        private IMapper _mapper
        {
            get;
        }
        public ContentService(IContentRepository contentRepository, IMapper mapper)
        {
            _driver = new ChromeDriver("c:/ChromeDriver");
            _mapper = mapper;
            _contentRepository = contentRepository;
        }

        public ResponseMessage? SearchContent(string term)
        {

            var responseMessage = new ResponseMessage { Description = "Houve um problema ao pesquisar o termo ou nenhum conteúdo foi encontrado;" };

            try
            {
                string url = "https://www.aec.com.br";
                _driver.Navigate().GoToUrl(url);
                if (ItIsPossibleToSearch(term))
                {
                    Thread.Sleep(5000);
                    var resultContent = TryFindElemntsByClass("text");
                    if (resultContent != null)
                    {
                        if (resultContent.Count <= 0) {
                            FinishWebDriver();
                            return responseMessage;
                        }
                        responseMessage.contentModel = GetContentData(resultContent);
                        responseMessage.Description = "Sucesso";
                        FinishWebDriver();

                    }
                }
                return responseMessage;

            }
            catch (Exception ex)
            {
                FinishWebDriver();
                responseMessage.Description += ex.Message.ToString();
                return responseMessage;
            }


        }

        private void FinishWebDriver()
        {
            _driver.Close();
            _driver.Quit();
        }

        private List<ContentModel> GetContentData(List<IWebElement> resultContent)
        {
            var ListContent = new List<ContentModel>();
            foreach (var element in resultContent)
            {
                var content = new ContentModel();
                content.ContentId = Guid.NewGuid();
                content.Area = element.FindElement(By.ClassName("hat")).Text;
                content.Titulo = element.FindElement(By.ClassName("tres-linhas")).Text;
                content.Descricao = element.FindElement(By.ClassName("duas-linhas")).Text;
                var publish = element.FindElement(By.XPath("span[2]/small")).Text;
                var publishInfo = publish.Split("em");
                content.Autor = publishInfo[0].Replace("Publicado por ", "");
                content.DataPublicacao = Convert.ToDateTime(publishInfo[1]);
                _contentRepository.SaveContent(_mapper.Map<Domain.Entities.Content>(content));


                ListContent.Add(content);
            }
            return ListContent;
        }


        private bool ItIsPossibleToSearch(string term)
        {
            Thread.Sleep(2000);
            var searchButtom = TryFindElemnt("//*[@id=\"header\"]/div[2]/div/div/div/div/ul[2]/li[2]/a");
            if (searchButtom == null)
                return false;
            searchButtom.Click();
            Thread.Sleep(2000);
            var inputSearch = TryFindElemnt("//*[@id=\"form\"]/input");
            if (inputSearch == null)
                return false;
            inputSearch.SendKeys(term + Keys.Enter);
            return true;
        }

        private IWebElement? TryFindElemnt(string xPath)
        {
            try
            {
                var element = _driver.FindElement(By.XPath(xPath));
                return element;
            }
            catch (NoSuchElementException)
            {
                return null;
            }
        }
        private List<IWebElement>? TryFindElemntsByClass(string className)
        {
            try
            {
                var elements = _driver.FindElements(By.ClassName(className)).ToList();
                return elements;
            }
            catch (NoSuchElementException)
            {
                return null;
            }
        }
    }
}
