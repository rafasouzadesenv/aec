﻿using Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Helper
{
    public class ResponseMessage
    {
        public string Description { get; set; }
        public List<ContentModel>? contentModel { get; set; } 
    }
}
