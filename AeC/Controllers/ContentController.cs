﻿using Application;
using Application.Helper;
using Application.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AeC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContentController : Controller
    {
        private IContentService _contentService;

        public ContentController(IContentService contentService)
        {
            _contentService = contentService;
        }

        [HttpGet("/GetContentByTerm")]
        public ResponseMessage GetAllTipoArranjo(string term)
        {

            return _contentService.SearchContent(term);

        }
    }
}
